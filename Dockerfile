FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./

# not sure what this is or does, but pi says I need it

# Dev
RUN npm install
ARG NODE_ENV=dev

# Prod?
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8443

CMD [ "node", "--experimental-modules", "main.js" ]


# The above Dockerfile was mostly copied from https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
